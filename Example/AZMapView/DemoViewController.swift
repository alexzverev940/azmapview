//
//  ViewController.swift
//  AZMapView
//
//  Created by Alex Zverev on 08.04.17.
//  Copyright © 2017 argas. All rights reserved.
//

import UIKit
import AZMapView

/// This is demo ViewController
///
class DemoViewController: UIViewController {
    
    private let mapViewAssembly = AZMapViewAssembly()
    private var mapView: AZMapView?
    
    private let heightOfBottomPanel: CGFloat = 48
    
    // uncomment scale factor to get tile size 256px
    private let tileSize = 256.0/*/UIScreen.main.scale*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMapView()
    }
    
    func configureMapView() {
        let tilesUrlConfig = AZTilesUrlConfig(url: "https://c.tile.thunderforest.com/cycle/",
                                              scale: 11,
                                              startXImageIndex: 1235,
                                              startYImageIndex: 636,
                                              imageExtension: ".png",
                                              apiKey: "a5dd6a2f1c934394bce6b0fb077203eb")
        let mapView = mapViewAssembly.mapView(xNumberOfTiles: 100,
                                              yNumberOfTiles: 100,
                                              tileSize: CGSize(width: tileSize, height: tileSize),
                                              tilesUrlConfig: tilesUrlConfig)
        mapView.frame = CGRect(x: 0, y: 0, width: 250, height: 250)
        mapView.center = view.center
        view.addSubview(mapView)
        self.mapView = mapView
    }
    
    // MARK: - IB ACTIONS
    
    /// It demonstrates, when mapView frame changes, mapView's tiles recalculates.
    @IBAction func fullScreenButtonAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            var frame = self.view.frame
            frame.size.height -= self.heightOfBottomPanel
            self.mapView?.frame = frame
            sender.isHidden = true
        }
    }
    
    @IBAction func showGridAction(_ sender: UIButton) {
        mapView?.displayGrid = !(mapView?.displayGrid ?? true)
        let title: String = (mapView?.displayGrid ?? true) ? "Hide grid" : "Show grid"
        sender.setTitle(title, for: .normal)
    }
    
}

