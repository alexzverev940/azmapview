# AZMapView

[![CI Status](http://img.shields.io/travis/Argas/AZMapView.svg?style=flat)](https://travis-ci.org/Argas/AZMapView)
[![Version](https://img.shields.io/cocoapods/v/AZMapView.svg?style=flat)](http://cocoapods.org/pods/AZMapView)
[![License](https://img.shields.io/cocoapods/l/AZMapView.svg?style=flat)](http://cocoapods.org/pods/AZMapView)
[![Platform](https://img.shields.io/cocoapods/p/AZMapView.svg?style=flat)](http://cocoapods.org/pods/AZMapView)

## About

**AZMapView** - ограниченный MapView, отображающий карту с сервиса [thunderforest](https://thunderforest.com/)

## Установка

**AZMapView** доступен через [CocoaPods](http://cocoapods.org). Для установки добавьте следующую строку в ваш Podfile:

```ruby
pod 'AZMapView', :git => 'https://alexzverev940@bitbucket.org/alexzverev940/azmapview.git'
```
## Как использовать

```
#!swift
// create assembly - is factory for mapView
let assembly = AZMapViewAssembly()
// create mapView by assembly
let mapView = assembly.mapView(xNumberOfTiles: 50, 
                               yNumberOfTiles: 50,
                                     tileSize: CGSize(width: 100, height: 100),
                               tilesUrlConfig: nil)
// place mapView to your view
mapView.frame = view.frame
view.addSubview(mapView)
```

Что бы настроить отображаемую область, следует задать параметр `tilesUrlConfig` (формирует ссылку верхнего для левого тайла карты) :
```
#!swift
AZTilesUrlConfig(url: "https://c.tile.thunderforest.com/cycle/",
               scale: 11,
    startXImageIndex: 1235,
    startYImageIndex: 636,
      imageExtension: ".png",
              apiKey: "yourApiKey") 
```


## Возможности

* Под коробкой SOA архитектура, верхний слой MVC c дополнительной сущностью tilesCoordinator. Core компоненты и сервисы легко подменить, т.к. закрыты протоколами.
* Dependency injection осуществляется ручками, через AZMapViewAssembly
* Создает лишь максимально отображаемое за раз число тайлов и переиспользует их 
* Изменяет число тайлов при изменении фрейма
* Асинхронно загружает тайлы и отменяет задачу загрузки если она уже не нужна
* Кеширует изображения тайлов на диск
* Есть пару тестов

## Author

Алексей Зверев, alexey-zverev@hotmail.com

## License

AZMapView is available under the MIT license. See the LICENSE file for more info.