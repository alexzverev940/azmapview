//
//  AZMapViewInteractor.swift
//  Pods
//
//  Created by Alex Zverev on 15.04.17.
//
//

import Foundation

protocol IAZMapViewInteractor {
    func loadImage(for displayingIndex: AZTileIndex,
                   initialIndex: AZTileIndex,
                   completion: @escaping (UIImage?, AZTileIndex) -> Void)
}

class AZMapViewInteractor: IAZMapViewInteractor {
    private let tileLoaderService: ITileLoaderService

    init(tileLoaderService: ITileLoaderService) {
        self.tileLoaderService = tileLoaderService
    }
    
    func loadImage(for displayingIndex: AZTileIndex,
                    initialIndex: AZTileIndex,
                    completion: @escaping (UIImage?, AZTileIndex) -> Void) {
        
        print("🎾 \(initialIndex)_\(displayingIndex): START")
        // stop previous image loading if needed
        tileLoaderService.preventLoading(for: initialIndex)

//        print("⏳ \(initialIndex)_\(displayingIndex): try to fetch")

        // try to fetch image from storage
        tileLoaderService
            .fetchImage(for: displayingIndex,
                        completionHandler: { (image, index) in
                            if let image = image {
//                                print("💾 \(initialIndex)_\(displayingIndex): FETCHED")
                                completion(image, index)
                            } else {
                                // load image if not existes
//                                print("⏳ \(initialIndex)_\(displayingIndex): start load")
                                self.loadImageFromServer(for: displayingIndex,
                                                         initialIndex: initialIndex,
                                                         completion: completion)
                            }
            })
    }
    
    private func loadImageFromServer(for displayingIndex: AZTileIndex,
                        initialIndex: AZTileIndex,
                        completion: @escaping (UIImage?, AZTileIndex) -> Void) {
        self.tileLoaderService
            .loadImage(for: displayingIndex,
                       initialIndex: initialIndex,
                       completionHandler: { (image: UIImage?, loadedTileIndex) in
//                        print("🎁 \(initialIndex)_\(displayingIndex): LOADED (\(loadedTileIndex))")
                        completion(image, loadedTileIndex)
            })
    }
    
}
