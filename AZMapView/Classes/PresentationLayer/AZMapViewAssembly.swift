//
//  AZMapViewAssembly.swift
//  AZMapView
//
//  Created by Alex Zverev on 08.04.17.
//  Copyright © 2017 argas. All rights reserved.
//

import UIKit

public class AZMapViewAssembly {
    
    public init() {}
    
    public func mapView(xNumberOfTiles: Int,
                 yNumberOfTiles: Int,
                 tileSize: CGSize,
                 tilesUrlConfig: AZTilesUrlConfig?) -> AZMapView {
        
        self.thunderForestUrlFactory = tilesUrlConfig ?? self.thunderForestUrlFactory
        let tilesCoordinator = AZTilesCoordinator(xNumberOfTiles: xNumberOfTiles,
                                                  yNumberOfTiles: yNumberOfTiles,
                                                  tileSize: tileSize)
        
        let view = AZMapView()
        let controller = AZMapController(tilesCoordinator: tilesCoordinator,
                                         interactor: mapViewInteractor(),
                                         mapView: view)
        view.controller = controller
        
        return view
    }
    
    private func mapViewInteractor() -> IAZMapViewInteractor {
        return AZMapViewInteractor(tileLoaderService: tileLoaderService)
    }
    
    private lazy var tileLoaderService: ITileLoaderService = {
        let loader = ImageDownloader()
        let cacher = Cacher()
        
        return TileLoaderService(imageLoader: loader,
                                 cacher: cacher,
                                 urlFactory: self.thunderForestUrlFactory)
    }()
    
    private lazy var thunderForestUrlFactory: AZTilesUrlConfig = {
        return AZTilesUrlConfig(url: "https://c.tile.thunderforest.com/cycle/",
                                scale: 11,
                                startXImageIndex: 1235,
                                startYImageIndex: 636,
                                imageExtension: ".png",
                                apiKey: "a5dd6a2f1c934394bce6b0fb077203eb")
    }()
    
}
