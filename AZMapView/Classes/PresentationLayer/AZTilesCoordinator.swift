//
//  AZTilesCoordinator.swift
//  AZMapView
//
//  Created by Alex Zverev on 08.04.17.
//  Copyright © 2017 argas. All rights reserved.
//

import UIKit

protocol AZTilesCoordinatorDelegate: class {
    func reuse(tile: AZTile, at index:AZTileIndex)
}

/// TilesCoordinnator represent a mapView as a large grid (for example 100x100 cells)
/// But it doesn't create 100x100 tiles, it creates that number of tiles, that can be displayed on the visible area.
/// While scroll change it's offset, tile coordinator change position of tiles, that are scrollView subviews.
/// When tile changes position it should display a data for different cell, and TilesCoordinnator tell to Redraw tile content via delgate.
class AZTilesCoordinator {
    
    weak var delegate: AZTilesCoordinatorDelegate?
    var scrollContentSize: CGSize
    
    private let xNumberOfTiles: Int
    private let yNumberOfTiles: Int
    private let tileSize: CGSize
    
    private var xTilesCount: Int = 0
    private var yTilesCount: Int = 0
    
    init(xNumberOfTiles: Int,
         yNumberOfTiles: Int,
         tileSize: CGSize) {
        self.xNumberOfTiles = xNumberOfTiles
        self.yNumberOfTiles = yNumberOfTiles
        self.tileSize = tileSize
        scrollContentSize = CGSize(width: CGFloat(xNumberOfTiles) * tileSize.width,
                                   height: CGFloat(yNumberOfTiles) * tileSize.height)

    }
    
    func createTiles(for visibleFrame: CGSize, displayGrid: Bool = false) -> [AZTile] {
        xTilesCount = Int(floor(visibleFrame.width/tileSize.width)) + 2
        yTilesCount = Int(floor(visibleFrame.height/tileSize.height)) + 2
        
        var tiles: [AZTile] = []
        for i in 0..<yTilesCount {
            for j in 0..<xTilesCount {
                let tileIndex = AZTileIndex(x: j, y: i)
                let tile = AZTile(initialIndex: tileIndex,
                                  size: tileSize)
                delegate?.reuse(tile: tile, at: tileIndex)
                tiles.append(tile)
            }
        }
        
        return tiles
    }
    
    func calculatePositions(tiles:[AZTile],
                            offset: CGPoint) {
        
        // number of  cells to the left of the visible area
        let xCellsBeforeOffset = floor(offset.x/tileSize.width)
        // number of  cells above the visible area
        let yCellsBeforeOffset = floor(offset.y/tileSize.height)
        
        // all tiles have their initial index
        // but when a tile is replaced, it gets new index
        // shows number of tiles that should be replaced
        let xTilesToBeReplaced = xCellsBeforeOffset.truncatingRemainder(dividingBy: CGFloat(xTilesCount))
        let yTilesToBeReplaced = yCellsBeforeOffset.truncatingRemainder(dividingBy: CGFloat(yTilesCount))
        
        // when all tiles replaced once - it's one tile cycle
        let xTilesCycles = Int(floor(xCellsBeforeOffset / CGFloat(xTilesCount)))
        let yTilesCycles = Int(floor(yCellsBeforeOffset / CGFloat(yTilesCount)))
        
        for tile in tiles {
            // soma tiles have +1 additinal cycle, as it is reused more
            let xFullCyclesForTile = CGFloat(tile.initialIndex.x+1) <= xTilesToBeReplaced ? xTilesCycles+1 : xTilesCycles
            let yFullCyclesForTile = CGFloat(tile.initialIndex.y+1) <= yTilesToBeReplaced ? yTilesCycles+1 : yTilesCycles
            
            // calculate actual tile position
            let actualTileX = CGFloat(xFullCyclesForTile) * CGFloat(xTilesCount) * tileSize.width + CGFloat(tile.initialIndex.x) * tileSize.width
            let actualTileY = CGFloat(yFullCyclesForTile) * CGFloat(yTilesCount) * tileSize.height + CGFloat(tile.initialIndex.y) * tileSize.height
            
            // if this current position isn't actual than change frame and call reuse method of delegate
            if tile.frame.origin.x != actualTileX ||
                tile.frame.origin.y != actualTileY {

                tile.frame.origin.x = actualTileX
                tile.frame.origin.y = actualTileY
                
                tile.displayedIndex = AZTileIndex(x: tile.initialIndex.x + xFullCyclesForTile*xTilesCount,
                                                  y: tile.initialIndex.y + yFullCyclesForTile*yTilesCount)
                delegate?.reuse(tile: tile,
                                at: tile.displayedIndex)
            }
            
        }

    }
    
}

