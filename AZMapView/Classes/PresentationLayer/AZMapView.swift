//
//  AZMapView.swift
//  AZMapView
//
//  Created by Alex Zverev on 08.04.17.
//  Copyright © 2017 argas. All rights reserved.
//

import UIKit

/// This class is limited MapView, working with `a.tile.thunderforest.com` service
/// Use AZMapViewBuilder to create an instance of this view
/// Use AZTilesUrlConfig to cofigure start url position of first tile from server
public class AZMapView: UIView {
    
    var controller: AZMapController?
    
    public var displayGrid: Bool = false {
        didSet {
            controller?.displayGrid = displayGrid
        }
    }
    
    override public var frame: CGRect {
        didSet {
            if oldValue.size != frame.size {
                controller?.update(frame: frame)
            }
        }
    }
    
    internal init() {
        super.init(frame: CGRect())
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
