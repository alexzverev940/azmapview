//
//  TilesDataSource.swift
//  Pods
//
//  Created by Alex Zverev on 13.04.17.
//
//

class AZMapController: NSObject, UIScrollViewDelegate, AZTilesCoordinatorDelegate {
    
    private unowned var mapView: AZMapView
    
    private let tilesCoordinator: AZTilesCoordinator
    private let interactor: IAZMapViewInteractor
    private let scrollView: UIScrollView
    private var tiles: [AZTile] = []

    var displayGrid: Bool = false {
        didSet {
            tiles.forEach({ $0.displayGrid = displayGrid })
        }
    }

    init(tilesCoordinator: AZTilesCoordinator,
         interactor: IAZMapViewInteractor,
         mapView: AZMapView) {
        scrollView = UIScrollView()
        self.tilesCoordinator = tilesCoordinator
        self.interactor = interactor
        self.mapView = mapView
        
        super.init()
        
        self.tilesCoordinator.delegate = self
        self.cofigureScroll(with: tilesCoordinator.scrollContentSize)
    }
    
    // MARK: -

    func update(frame: CGRect) {
        scrollView.frame = CGRect(origin: CGPoint.zero, size: frame.size)
        tiles.forEach({ $0.removeFromSuperview() })
        tiles = tilesCoordinator.createTiles(for: frame.size, displayGrid: displayGrid)
        tiles.forEach({ scrollView.addSubview($0) })
        tilesCoordinator.calculatePositions(tiles: tiles, offset: scrollView.contentOffset)
        print(" 🎾 frame: \(frame)")
    }
    
    private func cofigureScroll(with contentSize: CGSize) {
        scrollView.contentSize = contentSize
        scrollView.delegate = self
        scrollView.bounces = false
        
        mapView.addSubview(scrollView)
    }
    // MARK: - UIScrollViewDelegate
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tilesCoordinator.calculatePositions(tiles: tiles, offset: scrollView.contentOffset)
    }
    
    // MARK: - AZTilesCoordinatorDelegate
    
    // TODO: вынести в TileLoaderService, либо отдельную зависимость !
    func reuse(tile: AZTile, at index:AZTileIndex) {
        
        print("REUSE \(tile.initialIndex)_\(tile.displayedIndex)")
        
        // fix displayedIndex of tile
        let displayedIndex = tile.displayedIndex
        let initialIndex = tile.initialIndex
        
        tile.image = nil
        interactor.loadImage(for: displayedIndex,
                             initialIndex: initialIndex) { (image: UIImage?, loadedIndex: AZTileIndex) in
//                                print("-  \(initialIndex)_\(displayedIndex): VC:recived (\(loadedIndex))")
//                                print("🆗 \(tile.initialIndex)_\(tile.displayedIndex): Tile index VC:recived (\(loadedIndex))")
                                if let image = image,
                                    tile.displayedIndex == loadedIndex {
                                    print("✅ \(initialIndex)_\(displayedIndex): VC:SETTED (\(loadedIndex))")
                                    DispatchQueue.main.async {
                                        tile.image = image
                                    }
                                }
        }
        
        
        
    }
}
