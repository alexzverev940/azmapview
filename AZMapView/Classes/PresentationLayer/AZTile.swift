//
//  AZTile.swift
//  AZMapView
//
//  Created by Alex Zverev on 08.04.17.
//  Copyright © 2017 argas. All rights reserved.
//

import UIKit

struct AZTileIndex: CustomStringConvertible {
    let x: Int
    let y: Int
    
    var description: String {
        return "\(x):\(y)"
    }
}

extension AZTileIndex: Equatable {
    static func == (lhs: AZTileIndex, rhs: AZTileIndex) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }
}

class AZTile: UIImageView {
    
    /// Initial tile index in Grid (real)
    let initialIndex: AZTileIndex
   
    /// index of cell whose content is displayed by tile
    var displayedIndex: AZTileIndex {
        didSet {
            if (displayGrid) {
                DispatchQueue.main.async {
                    self.displayIndexLabel.text = "\(self.displayedIndex)"
                }
            }
        }
    }
    
    /// it true - indexes and grid will be displayed
    var displayGrid: Bool = false {
        didSet {
            DispatchQueue.main.async {
                self.realIndexLabel.isHidden = !self.displayGrid
                self.displayIndexLabel.isHidden = !self.displayGrid
                self.layer.borderWidth = self.displayGrid ? 1 : 0
            }
        }
    }
    
    private lazy var realIndexLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 8)
        label.textColor = UIColor.black
        label.frame = CGRect(x: 5, y: 5, width: self.frame.size.width, height: 12)
        label.text = "realIndex: \(self.initialIndex)"
        label.isHidden = true
     
        return label
    }()
    
    private lazy var displayIndexLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = UIColor.red
        label.frame = CGRect(x: 5, y: 17, width: self.frame.size.width, height: 12)
        label.text = "\(self.initialIndex)"
        label.isHidden = true
        
        return label
    }()
    
    required init(initialIndex: AZTileIndex,
                  size: CGSize) {
        
        self.initialIndex = initialIndex
        self.displayedIndex = initialIndex
        super.init(frame: CGRect(origin: CGPoint(x: CGFloat(initialIndex.x) * size.width,
                                                 y: CGFloat(initialIndex.y) * size.height),
                                 size: size))
        
        self.backgroundColor = UIColor.white
        
        addSubview(realIndexLabel)
        addSubview(displayIndexLabel)
        layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @available(*, unavailable, message: "Use init(gridPosition:width:height) instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("\(#file) init(coder:) has not been implemented")
    }
    
}
