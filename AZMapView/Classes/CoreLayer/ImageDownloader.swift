//
//  ImageDownloader.swift
//  AZMapView
//
//  Created by Alex Zverev on 09.04.17.
//  Copyright © 2017 argas. All rights reserved.
//

import UIKit

protocol IImageLoader {
    func loadImage(url urlString: String,
                   cancelationKey: String,
                   completionHandler: @escaping (UIImage?, Error?) -> Void)
    func cancelImagleLoad(cancelationKey: String)
}

class ImageDownloader: NSObject, IImageLoader, URLSessionDelegate {
    
    private var session: URLSession
    private var tasks: [String : URLSessionTask] = [:]
    
    override init() {
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = nil
        configuration.httpShouldUsePipelining = true
        configuration.httpMaximumConnectionsPerHost = 4

        session = URLSession(configuration: configuration)

        super.init()
    }
    
    func loadImage(url urlString: String,
                   cancelationKey: String,
                   completionHandler: @escaping (UIImage?, Error?) -> Void) {
        guard let url = URL(string: urlString) else {
            completionHandler(nil, NSError())
            return
        }
        let task = session.dataTask(with: url) { (
            data: Data?,
            response: URLResponse?,
            error:Error?) in
            
            if let error = error as? URLError,
                error.code == .cancelled {
                return
            }
//                print("TASK: cleaned \(cancelationKey)")
            self.tasks[cancelationKey] = nil
            
            if let error = error {
                completionHandler(nil, error)
            } else if let data = data {
                let image = UIImage(data: data)
                completionHandler(image, error)
            } else {
                completionHandler(nil, nil)
            }
            
        }
        
        print("TASK: add \(cancelationKey)")
        self.tasks[cancelationKey] = task
        
        task.resume()
    }
    
    func cancelImagleLoad(cancelationKey: String) {
        if (self.tasks[cancelationKey] != nil) {
            print("TASK: CANCEL \(cancelationKey)")
            self.tasks[cancelationKey]?.cancel()
            self.tasks[cancelationKey] = nil
        }
    }
}
