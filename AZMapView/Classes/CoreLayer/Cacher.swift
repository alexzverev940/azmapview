//
//  Cacher.swift
//  AZMapView
//
//  Created by Alex Zverev on 09.04.17.
//  Copyright © 2017 argas. All rights reserved.
//

import UIKit

protocol ICacher {
    func save(image: UIImage, with name: String)
    func fetchImage(name: String,
                    completionHandler: @escaping (UIImage?) -> Void)
}

class Cacher: ICacher {
    
    private let fileManager = FileManager.default
    private let writeQueue = DispatchQueue(label: "com.azmapview.writeQueue")
    
    private lazy var cachePath: String = {
        let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        return cacheDirectory + "/com.azmapview.tiles"
    }()

    init() {
        if !fileManager.fileExists(atPath: cachePath) {
            do {
                try fileManager.createDirectory(atPath: cachePath, withIntermediateDirectories: true, attributes: nil)
            } catch _ {
            }
        }
    }
    
    func save(image: UIImage, with name: String) {
        writeQueue.async {
            let data: Data? = UIImageJPEGRepresentation(image, 1)
            try? data?.write(to: URL(fileURLWithPath: self.path(for: name)), options: [])
        }
    }
    
    func fetchImage(name: String, completionHandler: @escaping (UIImage?) -> Void) {
        DispatchQueue.global(qos: .userInteractive).async {
            if let data = (try? Data(contentsOf: URL(fileURLWithPath: self.path(for: name)))) {
                completionHandler(UIImage(data: data))
            } else {
                completionHandler(nil)
            }
        }
    }
    
    private func path(for imageName: String) -> String {
        return cachePath + "/" + imageName.replacingOccurrences(of: "/", with: "_")
    }
}
