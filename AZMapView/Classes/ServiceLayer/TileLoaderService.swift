//
//  ITileLoaderService.swift
//  AZMapView
//
//  Created by Alex Zverev on 08.04.17.
//  Copyright © 2017 argas. All rights reserved.
//

import UIKit

protocol ITileLoaderService {
        
    /// Fetch image assosiated with tileIndex from cache
    func fetchImage(for tileIndex: AZTileIndex,
                     completionHandler: @escaping (UIImage?, AZTileIndex) -> Void)
    
    /// Load image assosiated with tileIndex from server
    func loadImage(for displayingIndex: AZTileIndex,
                   initialIndex: AZTileIndex,
                   completionHandler: @escaping (UIImage?, AZTileIndex) -> Void)
    
    /// Prevent operation of loading image for tile by index. If needed.
    func preventLoading(for tileIndex: AZTileIndex)
}

class TileLoaderService: ITileLoaderService {
    
    private let imageLoader: IImageLoader
    private let cacher: ICacher
    private let urlFactory: AZTilesUrlConfig
    
    required init(imageLoader: IImageLoader,
                  cacher: ICacher,
                  urlFactory: AZTilesUrlConfig) {
        self.imageLoader = imageLoader
        self.cacher = cacher
        self.urlFactory = urlFactory
    }
    
    func fetchImage(for tileIndex: AZTileIndex,
                    completionHandler: @escaping (UIImage?, AZTileIndex) -> Void) {
        cacher.fetchImage(name: urlFactory.url(for: tileIndex)) { (image) in
            completionHandler(image, tileIndex)
        }
    }
    
    func loadImage(for displayingIndex: AZTileIndex,
                   initialIndex: AZTileIndex,
                   completionHandler: @escaping (UIImage?, AZTileIndex) -> Void) {
        let imageUrl = urlFactory.url(for: displayingIndex)
        imageLoader.loadImage(url: imageUrl,
                              cancelationKey: "\(initialIndex)") { (image, _) in
                                if let image = image {
                                    self.cacher.save(image: image, with: imageUrl)
                                }
                                completionHandler(image, displayingIndex)
        }
    }
    
    func preventLoading(for tileIndex: AZTileIndex) {
        imageLoader.cancelImagleLoad(cancelationKey: "\(tileIndex)")       
    }
}



