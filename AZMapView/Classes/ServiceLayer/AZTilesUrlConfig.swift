//
//  AZTilesUrlFactory.swift
//  AZMapView
//
//  Created by Alex Zverev on 09.04.17.
//  Copyright © 2017 argas. All rights reserved.
//


/// Make url for tile
/// https://a.tile.thunderforest.com/cycle/14/9889/5103.png?apikey=a5dd6a2f1c934394bce6b0fb077203eb
///
public struct AZTilesUrlConfig {
    private let url: String // = "https://c.tile.thunderforest.com/cycle/"
    private let scale: Int // = 11
    private let startXImageIndex: Int// = 1235
    private let startYImageIndex: Int // = 636
    private let imageExtension: String // = ".png"
    private let apiKey: String? // = "a5dd6a2f1c934394bce6b0fb077203eb"
    
    public init(url: String,
         scale: Int,
         startXImageIndex: Int,
         startYImageIndex: Int,
         imageExtension: String,
         apiKey: String?) {
        self.url = url
        self.scale = scale
        self.startYImageIndex = startYImageIndex
        self.startXImageIndex = startXImageIndex
        self.imageExtension = imageExtension
        self.apiKey = apiKey
    }
    
    /// Generate url for tile index
    ///
    /// - Returns: url like `https://a.tile.thunderforest.com/cycle/14/9889/5103.png?apikey=a5dd6a2f1c934394bce6b0fb077203eb`
    func url(for tileIndex: AZTileIndex) -> String {
        var result =  url
            + "\(scale)/"
            + "\(startXImageIndex + tileIndex.x)/"
            + "\(startYImageIndex + tileIndex.y).png"
        if let apiKey = apiKey {
            result += "?apikey=\(apiKey)"
        }
        
        return result
    }
}
